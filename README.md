<!--

SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>

SPDX-License-Identifier: MIT

-->

# Tmux configs

`tmux` is a [terminal multiplex for UNIX](https://en.wikipedia.org/wiki/Tmux).

## Main (default)

[`./tmux.conf`](./tmux.conf).

### Usage

To set as the **_default_ tmux config** (`dotfile`):

```sh
$ cp tmux.conf $HOME/.tmux.conf
```
Or start a tmux session like this:

```sh
$ tmux -f /path/to/tmux.conf
```

## Alternatives and extensions:
<!-- $ tree {ext*,arc*,alt*} -->

```sh
extend
└── tmux.svbaelen.conf
archive
└── tmux.old.conf (version < 2.9)
alternatives
└── tmux.example.conf (default example in tmux package)
```

## Awesome tmux

Much more awesome tmux-joy to be found at 
[https://github.com/rothgar/awesome-tmux](https://github.com/rothgar/awesome-tmux).

:warning: WARNING: you might just get lost out there, and realise 5 days later you missed all your important deadlines.

Have fun!
